# Preseed Cinnamon Ubuntu

**HINT**: Regard this thing as a template, please adjust to your needs (packages,localizations,...) 

This will preseed a minimal Ubuntu plus Cinnamon desktop.

During preseed, it will ask for:

* hostname

After installation has been finished, the encryption key and user (_morph_) password is ```insecure```

You can change the encryption password like so:

```
sudo cryptsetup luksChangeKey /dev/sda3
```

## PXE setup

In your PXE server setup, you need to add something like this:

```
label install
        menu label Preseed Cinnamon Ubuntu
        kernel ubuntu-installer/xenial/linux
        append preseed/url=http://some.host/preseed/crypto-xenial-cinnamon.seed initrd=ubuntu-installer/xenial/initrd.gz locale=de_DE.UTF-8 debian/priority=critical vga=normal debian-installer/keymap=de console-keymaps-at/keymap=de console-setup/layoutcode=de_DE netcfg/choose_interface=auto localechooser/translation/warn-light=true localechooser/translation/warn-severe=true console-setup/ask_detect=false FRONTEND_BACKGROUND=original --
```

Example using this repo (adjust to your fork, if you want):

:exclamation: Disk will be wiped without asking :exclamation:

```
label install
        menu label Preseed Cinnamon Ubuntu (DISK WILL BE WIPED!)
        kernel ubuntu-installer/xenial/linux
        append preseed/url=https://gitlab.com/morph027/preseed-cinnamon-ubuntu/raw/xenial/crypto-xenial-cinnamon.seed debian-installer/allow_unauthenticated_ssl=true initrd=ubuntu-installer/xenial/initrd.gz locale=de_DE.UTF-8 debian/priority=critical vga=normal debian-installer/keymap=de console-keymaps-at/keymap=de console-setup/layoutcode=de_DE netcfg/choose_interface=auto localechooser/translation/warn-light=true localechooser/translation/warn-severe=true console-setup/ask_detect=false FRONTEND_BACKGROUND=original --
```

If you want to change partition settings on your own, use this:

```
label install
        menu label Preseed Cinnamon Ubuntu
        kernel ubuntu-installer/xenial/linux
        append preseed/url=https://gitlab.com/morph027/preseed-cinnamon-ubuntu/raw/xenial/xenial-cinnamon.seed debian-installer/allow_unauthenticated_ssl=true initrd=ubuntu-installer/xenial/initrd.gz locale=de_DE.UTF-8 debian/priority=critical vga=normal debian-installer/keymap=de console-keymaps-at/keymap=de console-setup/layoutcode=de_DE netcfg/choose_interface=auto localechooser/translation/warn-light=true localechooser/translation/warn-severe=true console-setup/ask_detect=false FRONTEND_BACKGROUND=original --
```

Also add installer files to PXE server:

```
cd /var/lib/tftpboot/ubuntu-installer
mkdir xenial
wget http://archive.ubuntu.com/ubuntu/dists/xenial/main/installer-amd64/current/images/netboot/netboot.tar.gz
tar xzf netboot.tar.gz 
rm -f netboot.tar.gz ldlinux.c32 pxelinux.0 pxelinux.cfg version.info
mv ubuntu-installer/amd64/linux ubuntu-installer/amd64/initrd.gz xenial/
rm -rf ubuntu-installer netboot.tar.gz ldlinux.c32 pxelinux.0 pxelinux.cfg version.info
```

## Extras

### apt-cacher-ng

As we are using an apt-cacher-ng host in our network, we'd like to use this for preseeding. Just look for the comments regarding apt-cacher-ng and adjust to your setup. Thats also the reason, why the sources list gets fetched in _late\_command_, cause preseeding will leave the "ugly" strings there. You could then switch to a setup using [apt-find-proxy](https://github.com/Tolaris/apt-find-proxy), which only uses the apt-cacher-ng proxy when reachable.
